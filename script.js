//MAPA do LABIRINTO
const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S       W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W     W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

const main = document.querySelector('#content')

for (let row = 0; row < map.length; row++) {

    let newRow = document.createElement('div')
    newRow.setAttribute('class', 'rows')

    for (let cell = 0; cell < map[row].length; cell++) {

        let newCell = document.createElement('div')
        let classCell = ""

        if (map[row][cell] === 'W') {

            classCell = 'cell parede'
        }

        if (map[row][cell] === ' ' || map[row][cell] === 'S') {

            classCell = 'cell caminho'
        }

        if (map[row][cell] === 'F') {

            classCell = 'cell finish'
        }

        newCell.setAttribute('class', classCell)
        newRow.appendChild(newCell)
    }

    newRow.setAttribute('id', row.toString())
    main.appendChild(newRow)
}

//row index start
let indexRow = 9
//cell index start
let indexCell = 0
//player
const player = document.createElement('div')
player.setAttribute('class', 'cell start')
//STEP do PLAYER
const stepPlayer = 1
//Div inicial do Player
const startIndexPlayer = getRowId(indexRow).children
startIndexPlayer[indexCell].appendChild(player)
//Gif finish successfully
let divGif = document.createElement('img')
divGif.setAttribute('class', 'gif')
divGif.setAttribute('src', 'https://media1.tenor.com/images/5e0f2d3d547968d84f9a11b203a985a9/tenor.gif?itemid=10037888')

function getRowId (idRow) {

    let id = idRow.toString()
    let row = document.getElementById(id)

    return row
}

function finish () {

    main.innerHTML = ''

    let divFinish = document.createElement('div')
    divFinish.setAttribute('class', 'div chegada')

    divFinish.appendChild(divGif)
    main.appendChild(divFinish)
}

function moveToRight() {

    indexCell += stepPlayer
    const newIndexRow = getRowId(indexRow).children

    if (newIndexRow[indexCell].className == 'cell finish') {

        finish()
    }

    if (newIndexRow[indexCell].className == 'cell caminho') {

        newIndexRow[indexCell].appendChild(player)
    }

    if (newIndexRow[indexCell].className == 'cell parede') {

        indexCell -= stepPlayer
    }
}

function moveToLeft() {

    indexCell -= stepPlayer
    const newIndexRow = getRowId(indexRow).children

    if (indexCell < 0) {

        indexCell = 0
    }

    if (newIndexRow[indexCell].className == 'cell caminho') {

        newIndexRow[indexCell].appendChild(player)
    }

    if (newIndexRow[indexCell].className == 'cell parede') {

        indexCell += stepPlayer
    }
}

function moveToUp() {

    indexRow -= stepPlayer
    const newIndexRow = getRowId(indexRow).children

    if (newIndexRow[indexCell].className == 'cell caminho') {

        newIndexRow[indexCell].appendChild(player)
    }

    if (newIndexRow[indexCell].className == 'cell parede') {

        indexRow += stepPlayer
    }
}

function moveToDown() {

    indexRow += stepPlayer
    const newIndexRow = getRowId(indexRow).children

    if (newIndexRow[indexCell].className == 'cell caminho') {

        newIndexRow[indexCell].appendChild(player)
    }
    if (newIndexRow[indexCell].className == 'cell parede') {

        indexRow -= stepPlayer
    }
}

//KEYDOWN EVENT para MOVIMENTAR o PLAYER
document.addEventListener('keydown', (event) => {

    if (event.key == 'ArrowRight') {

        moveToRight()
    }

    if (event.key == 'ArrowLeft') {

        moveToLeft()
    }

    if (event.key == 'ArrowUp') {

        moveToUp()
    }

    if (event.key == 'ArrowDown') { 

        moveToDown()
    }
})